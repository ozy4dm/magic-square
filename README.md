# 魔方陣を生成するC++のコードを出力するプログラム(Ruby) #

ruby -Ku generate.rb [オプション] < 探索順序を書いたテキストファイル > C++のソースファイル.cpp

みたいな感じでコードを生成し、それをコンパイル・実行してください。
詳細はソースコードのコメントに書いてあります。

### ◎　CodeIQの問題「魔方陣ヌルヌル」　###

タテ・ヨコ・ナナメそれぞれの合計がすべて0になる、魔方陣ヌルヌルを生成する場合は、

* n=3の場合
ruby -Ku generate.rb -nsy < path3.txt > mn3.cpp


* n=4の場合
ruby -Ku generate.rb -nsy < path4b.txt > mn4.cpp


* n=5の場合
ruby -Ku generate.rb -nsy < path5b.txt > mn5.cpp


のようにしてソースコードを生成してください。

### ◎　全列挙プログラムの生成　###

* n=3の場合
ruby -Ku generate.rb -esn < path3.txt > me3.cpp

* n=4の場合
ruby -Ku generate.rb -esn < path4b.txt > me4.cpp

* n=5の場合
ruby -Ku generate.rb -esn < path5b.txt > me5.cpp


4×4の場合、path4a.txtを使うと時間がかかります。
5×5の場合は、path5a.txtを使っても遅すぎて解けません。
列挙プログラムは特別な枝刈りを行っていないため、-fopenmpオプションで並列化しておかないと非常に時間がかかります。

* コンパイル例
g++ -O3 -fopenmp me5.cpp
