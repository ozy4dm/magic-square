# coding; utf-8

# 
# CodeGenerator.new( block )
# 
# みたいに使用する。blockは、
# {
#   :path => 探索ルート（0からn*n-1までのインデックスの配列）
#   :enum => 解を列挙するか（trueなら全列挙、falseなら1つ解が見つかった時点で終了）
#   :null => ヌルヌル魔方陣にするか（trueならヌルヌル、falseなら通常の魔方陣）
#   :show => 解を表示するかどうか（速度を調べるための列挙ならfalseにしておいた方が良い）
#   :sep  => 解を列挙する場合に、:sep個ごとで経過時間を表示する
# }
# のようになっている。
# 
# 出力するC++のコードは、ルート順に値を決めていき、ビンゴした時点で
# 探索を継続するか諦めるかの判断をしているだけ。
# どのマス目を埋めたかは、32/64bit符号なし整数を用いているため、8×8魔方陣までしか対応していない。
# （というより、計算量的にそこまで大きいと求まらない。せいぜい6×6が限度だろう）
# 
# 4×4、5×5、6×6あたりで、探索順序によって列挙の速度が全く異なることを確認する目的で
# 書いたプログラマので、高速列挙するなら自分で書いた方が良い。
# 
# 本プログラムも、回転・対象形を省く処理くらいは入れておいた方が良いかも知れない。
# 

class CodeGenerator
  
  def initialize( argv )
    @path = argv[:path]
    @show = argv[:show]
    @null = argv[:null]
    @enum = argv[:enum]
    @sep = argv[:sep]
    
    @m = @path.size
    @n = Math.sqrt(@m).to_i
    # 型名は適当なので、環境に合わせて適当に書き替えるべき
    @ftype = @m<31 ? 'unsigned long' : 'unsigned long long'
    if @n*@n!=@m
      puts"invalid path"
      exit
    end
    
    print_main_code
    generate
  end
  
  # 
  # ヘッダとかメイン部分。
  # 後から適当に並列化したためr0は必要なくなっている。
  # 
  def print_main_code
    
    puts <<"HEADER"
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <ctime>

const int total = #{(@m+1)*@m/2/@n};
unsigned long count=0;
time_t start, end;

#define SUM#{@n-1}(#{(1..@n-1).map{|k|'x'+k.to_s}.join(',')}) (#{(1..@n-1).map{|k|'v[x'+k.to_s+']'}.join('+')})
#define SUM#{@n}(#{(1..@n).map{|k|'x'+k.to_s}.join(',')}) (#{(1..@n).map{|k|'v[x'+k.to_s+']'}.join('+')})

HEADER

puts"// プロトタイプ宣言"
(0..@m).map{|k|puts"void r#{k}(char*v, #{@ftype} f);"}

puts <<"SOLVE"

void solve()
{
  /*
      mマスの方陣の場合はm個の作業用メモリを確保し、
      OpenMPによる手抜き並列化を行う。
  */
  char**v = new char*[#{@m}];
  for(int i=0; i<#{@m}; ++i){
    v[i] = new char[#{@m}]();
  }
  
  #pragma omp parallel for
  for(int i=1; i<=#{@m}; ++i){
    v[i-1][#{@path[0]}]=i;
    r1(v[i-1], 1LL<<(i-1));
  }
  
  for(int i=0; i<#{@m}; ++i){
    delete[] v[i];
  }
  delete[] v;
  
  std::cout<<\"Total: \"<<count<<std::endl;
}

SOLVE

puts <<"MAIN"
int main()
{
  time(&start);
  solve();
  return 0;
}

MAIN

  end
  
  
  # 
  # 
  # 
  def generate
    ltate = []
    lyoko = []
    @n.times{|i|
      t = i
      v = []; w = []
      @n.times{|j|
        v << t; t+=@n
        w << @n * i + j
      }
      ltate << v
      lyoko << w
    }
    
    lnaname = []
    lnaname << (0..@n-1).map{|i| (@n+1) * i }
    lnaname << (0..@n-1).map{|i| (@n-1) * (i+1) }
    
    @lines = ltate + lyoko + lnaname
    
    q = Array.new(@m, false)
    @path.each_with_index{|k,idx|
      q[k] = true
      complines = check_line(q)
      
      puts"void r#{idx}(char*v, #{@ftype} f)
{"
      
      if complines.size > 0
        #print"#{k}: "
        #p complines
        puts"  for(int i=1; i<=#{@m}; ++i){"
        complines.map{|v|
          w = v.dup
          w.delete(k)
          puts"    if( (f&(1LL<<(i-1))) || SUM#{@n-1}(#{w.join(',')})+i!=total ) continue;"
          @lines.delete(v)
        }
        puts"    v[#{k}]=i; r#{idx+1}(v,f|(1LL<<(i-1)));";
        puts"  }"
      else
        puts"for(int i=1; i<=#{@m}; ++i){
    if( (f&(1LL<<(i-1))) ) continue;
    v[#{k}]=i; r#{idx+1}(v,f|(1LL<<(i-1)));
  }"
      end
      puts"}"
    }
    
puts <<"PRINTV"

void printv(char*v)
{
#{@show ? '' : '/*'}
  for(int i=0; i<#{@m}; ++i){
    std::cout << std::setw(#{@m.to_s.size+1}) << (int)#{
      if @null
        if @n%2==1
          'v[i]-'+((@m+1)*@m/2/@n/@n).to_s
        else
          'v[i]*2-'+((@m+1)*@m/@n/@n).to_s
        end
      else
        'v[i]'
      end
    } << ( (i+1)%#{@n} ? \" \" : \"\\n\" );
  }
  std::cout<<std::endl;
#{@show ? '' : '*/'}
  #{@enum ? '' : 'exit(0);' }
}

PRINTV

puts <<"LAST"    
void r#{@m}(char*v, #{@ftype} f)
{
  #pragma omp critical
  {
    ++count;
    printv(v);
    if(count%#{@sep}==0){
      time(&end);
      std::cout<<count<<"("<<(end-start)<<"s)"<<std::endl;
    }
  }
}
LAST
  end
  
  def check_line(q)
    complines = []
    @lines.map{|v|
      ok = true
      v.map{|k|
        next if q[k]
        ok = false
        break
      }
      complines << v if ok
    }
    complines
  end
end
