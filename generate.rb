# coding; utf-8

# 
# ruby generate.rb [switches] < pathdata.txt > magic.cpp
# 
# みたいなかんじで、探索順序を標準入力に与えてください。
# オプションは次の通りです。
# 
#   -e, --enum       すべての魔方陣を列挙します
#   -n, --null       ヌルヌル魔方陣（合計が0になる魔方陣）を作ります
#   -s, --show[y|n]  魔方陣を表示／非表示の設定です。列挙の速度だけ調べる場合は非表示の方が良いです。
#   -c, --count[n]   魔方陣がn個見つかったところで経過時間（秒）が表示されます（例）-c1000
# 
# 標準出力をC++のコードとしてコンパイル・実行してください。
# （gcc 4.8.2でしか確認してませんので、動かない場合は適当に修正してください）
# 
# gcc -O3 magic.cpp -fopenmp
# 
# とかすると、並列化されて速くなります。
# 自動生成のコードなので、ものすごく速いというわけではありません。
# 本プログラムで探索順序を調べてみて、「これは速い！」という順序が見つかったら
# 自分で書き直してみるのが良いかと思います。
# 
# 探索順序のインデックスは、たとえば4×4の魔方陣なら、
# 
#   0  1  2  3
#   4  5  6  7
#   8  9 10 11
#  12 13 14 15
# 
# のように、方陣の左上を0として右下方向に増えるようになっています。探索順序の書き方は、
# path4a.txt, path4b.txt, path5s.txt, path5b.txt, path6a.txt, path6b.txt
# を参考にしてください。
# 
# 全列挙は、4×4は可能です。
# 5×5は、path5b.txtの順序なら、できなくはないですが、非常に時間がかかります。
# 6×6は、現実的な時間では不可能です。
# path6aはおそらく1つも解が得られません。path6bならいくつか解は得られますが、少し時間がかかります。
# 

require 'optparse'

load 'CodeGenerator.rb'

argv = {
  :path => STDIN.readlines.join.split.map{|s|s.to_i},
  :enum => false,
  :null => false,
  :show => true,
  :sep => 10000
}

opt = OptionParser.new
opt.on('-e', '--enum', '--enumerate'){|v|
  argv[:enum] = true
}
opt.on('-n', '--null', '--null2'){|v|
  argv[:null] = true
}
opt.on('-s', '--show SW'){|v|
  argv[:show] = v=='y' ? true : false
}
opt.on('-c', '--count COUNT_SEP'){|v|
  n = v.to_i
  argv[:sep] = [1, n].max
}
opt.parse!(ARGV)

path = argv[:path]
m = path.size
n = Math.sqrt(m).to_i

if m != n*n
  STDERR.puts"Invalid path's length #{m}:
allowed [ 9, 16, 25, 36, 49, 64 ]"
  exit
end

if m != path.uniq.size
  h = Hash.new
  STDERR.puts"Node's name is not unique:
#{path.group_by{|d|d}.map{|t|'node'+t[0].to_s+':'+t[1].size.to_s}.join(', ')}"
  exit
end

if (0..m-1).to_a != path.sort
  STDERR.puts"Invalid node name:
allowed #{(0..m-1).to_a.join(', ')}"
  exit
end

STDERR.puts <<"PARAM"
Enumerate: #{argv[:enum] ? 'yes' : 'no'}
Show Solutions: #{argv[:show] ? 'yes' : 'no'}
NullNull: #{argv[:null] ? 'yes' : 'no'}
Print exec time(s) every #{argv[:sep]} solutions
Magic Square's size: #{n}
Search Path:
#{argv[:path].join(', ')}(#{m}nodes)

Please compile as C++: g++ -O3 [-fopenmp] magic.cpp
PARAM

CodeGenerator.new(argv)
